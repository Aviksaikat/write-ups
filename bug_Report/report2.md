# Report 2 : Clickjacking

---

- Make a html file or use the html file below and load the website http://minet.co/ inside an iframe element.
```xml
<!DOCTYPE html>
<html>
	<h1> Clickjacking </h1>
	<iframe src="https://minet.co/" width="1000" height="800"></iframe>
</html>
```

- ![](images/5.png)

- CVSS score 
- ![](images/12.png)