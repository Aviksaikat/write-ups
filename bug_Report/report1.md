# Report 1 : Host header injection 

---

- First turn on burpsuite/zap and go to http://minet.co/.
- Then send this request to the repeater.
- ![](images/1.png)
- Then change the host to any website. I'm using bing.com and send the request 
- ![](images/2.png)
- So we get a `302` response with the location of `bing.com`
- ![](images/3.png)

- CVSS score 
- ![](images/4.png)