# Report 3 : No Valid SPF Record leads to E-Mail Spoofing

---

- Go to https://www.kitterman.com/spf/validate.html then enter the domain name `minet.co` and press `Get SPF Record`.
- ![](images/6.png)
- We get a response back saying `No valid SPF record found.`
- ![](images/7.png)
- To see it's impact now go to https://emkei.cz/ and create a mail using the domain name as `minet.co`
- ![](images/8.png)
- We got a mail form `hacker@minet.co`
- ![](images/9.png)

- CVSS score
- ![](images/10.png)